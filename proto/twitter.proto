syntax = "proto3";

package twitter;

import "tweet.proto";

option go_package = "proto";

service Twitter {
  // Query the Twitter Tweet Search REST API.
  // TODO(jv): Implement other REST endpoints as needed.
  rpc SearchTweets(SearchTweetsArgs) returns (SearchTweetsRet) {}
  
  // Query the StreamService.Filter streaming API with the given parameters.
  rpc StreamTweets(StreamTweetsArgs) returns (stream StreamTweetsRet) {}
}

/*
=REST API Tweet Search request parameters defn
DETAILS(https://dev.twitter.com/rest/reference/get/search/tweets)

>query
A UTF-8, URL-encoded search query of 500 characters maximum, including
operators.
@noradio

>geocode
Returns tweets by users located within a given radius of the given
latitude/longitude. The location is preferentially taking from the Geotagging
API, but will fall back to their Twitter profile. The parameter value is
specified by ” latitude,longitude,radius ”, where radius units must be
specified as either ” mi ” (miles) or ” km ” (kilometers).
37.781157 -122.398720 1mi

>lang
Restricts tweets to the given language, given by an ISO 639-1 code. Language
detection is best-effort.

>locale
Specify the language of the query you are sending (only ja is currently
effective)

>result_type
Optional. Specifies what type of search results you would prefer to receive.
The current default is “mixed.” Valid values include:
mixed : Include both popular and real time results in the response.
recent : return only the most recent results in the response.
popular : return only the most popular results in the response.

>count
The number of tweets to return per page, up to a maximum of 100. Defaults to
15.

>since
Returns tweets created before the given date. Date should be formatted as
YYYY-MM-DD. Keep in mind that the search index has a 7-day limit. In other
words, no tweets will be found for a date older than one week.

>since_id
Returns results with an ID greater than (that is, more recent than) the
specified ID. There are limits to the number of Tweets which can be accessed
through the API. If the limit of Tweets has occurred since the since_id, the
since_id will be forced to the oldest ID available.

>max_id
Returns results with an ID less than (that is, older than) or equal to the
specified ID.

>include_entities
The entities node will not be included when set to false.
*/
message SearchTweetsArgs {
  // @inject_tag: url:"q,omitempty"
  string query = 1;
  // @inject_tag: url:"geocode,omitempty"
  string geocode = 2;
  // @inject_tag: url:"lang,omitempty"
  string lang = 3;
  // @inject_tag: url:"locale,omitempty"
  string locale = 4;
  // @inject_tag: url:"result_type,omitempty"
  string result_type = 5;
  // @inject_tag: url:"count,omitempty"
  int32 count = 6;
  // @inject_tag: url:"since,omitempty"
  string since = 7;
  // @inject_tag: url:"since_id,omitempty"
  int64 since_id = 8;
  // @inject_tag: url:"max_id,omitempty"
  int64 max_id = 9;
  // @inject_tag: url:"include_entities,omitempty"
  bool include_entities = 10;
}

message SearchTweetsRet {
  message SearchMetadata {
    double completed_in = 1;
    int64 max_id = 2;
    string query = 3;
    int32 count = 4;
    int64 since_id = 5;
  }

  SearchMetadata search_metadata = 1;
  repeated twitter.Tweet statuses = 2;
}

/*
=Streaming API request parameters defn
DETAILS(https://dev.twitter.com/streaming/overview/request-parameters)

>filter_level
Setting this parameter to one of none, low, or medium will set the minimum
value of the filter_level Tweet attribute required to be included in the
stream. The default value is none.

>stall_warnings
Setting this parameter to the string true will cause periodic messages to be
delivered if the client is in danger of being disconnected. These messages are
only sent when the client is falling behind, and will occur at a maximum rate
of about once every 5 minutes.

>follow
A comma-separated list of user IDs, indicating the users whose Tweets should be
delivered on the stream

>track
A comma-separated list of phrases which will be used to determine what Tweets
will be delivered on the stream. A phrase may be one or more terms separated by
spaces, and a phrase will match if all of the terms in the phrase are present
in the Tweet, regardless of order and ignoring case. By this model, you can
think of commas as logical ORs, while spaces are equivalent to logical ANDs
(e.g. ‘the twitter’ is the AND twitter, and ‘the,twitter’ is the OR twitter).

>languages
Setting this parameter to a comma-separated list of BCP 47 language identifiers
corresponding to any of the languages listed on Twitter’s advanced search page
will only return Tweets that have been detected as being written in the
specified languages. For example, connecting with language=en will only stream
Tweets detected to be in the English language.

>locations
A comma-separated list of longitude,latitude pairs specifying a set of bounding
boxes to filter Tweets by. Only geolocated Tweets falling within the requested
bounding boxes will be included—unlike the Search API, the user’s location
field is not used to filter tweets.

Each bounding BOX should be specified as a pair of longitude and latitude
pairs, with the southwest corner of the bounding box coming first.
-122.75,36.8,-121.75,37.8,-74,40,-73,41 = (San Francisco OR New York City)
*/

message StreamTweetsArgs {
  // @inject_tag: url:"filter_level,omitempty"
  string filter_level = 1; 
  // @inject_tag: url:"stall_warnings,omitempty"
  bool stall_warnings = 2;
  // @inject_tag: url:"follow,omitempty,comma"
  repeated string follow = 3;
  // @inject_tag: url:"track,omitempty,comma"
  repeated string track = 4;
  // @inject_tag: url:"language,omitempty,comma"
  repeated string language = 5;
  // @inject_tag: url:"locations,omitempty,comma"
  repeated string locations = 6;
}

message StreamTweetsRet {
  twitter.Tweet status= 1;
}
