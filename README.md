## Disclaimer
The intention of this is largely to serve as a reference rather than a true
import or application. This is by design a small component that composes larger
systems which are orchestrated by some higher level mechanism. For example 
containerizing with [Docker](https://www.docker.com/) and deploying and managing
the container cluster(s) with [Kubernetes](http://kubernetes.io/). The plan is to
eventually provide an example of such an deployment as I open source enough components
to perform some useful and cool actions. Towards this end, the orchestration
example may prove more directly applicable once available; or at the very least more
complete examples of how one might want to create and manage a micro-service-like environment.

# Twitter Service

[GoDoc]: https://godoc.org/bitbucket.org/jvulic/twitter
[GoDoc Widget]: https://godoc.org/bitbucket.org/jvulic/twitter?status.png
[Go Report Card]: https://goreportcard.com/report/bitbucket.org/jvulic/twitter
[Go Report Card Widget]: https://goreportcard.com/badge/bitbucket.org/jvulic/twitter

[![Go Doc][GoDoc Widget]][GoDoc] [![Go Report Card][Go Report Card Widget]][Go Report Card]

## Introduction

Twitter is a small service that provides a means of accessing a subset of
Twitter REST and Streaming APIs through rpc requests. This could be extended to
support the entire Twitter API by design, though plans to add functionality is
on a *need-by-need* basis.

The RPC framework is provided by [grpc](http://www.grpc.io/) as implemented in
Go. As consequence the underlying rpc encoding used is [Protocol
Buffers](https://developers.google.com/protocol-buffers/).

The service implementation provides both a means of starting the service and
performing operations on the service at runtime. See [Usage](#usage) for
details.

To deploy this service, you will need to provide either Twitter *User
Authentication* for full support, or *Application-only authentication* if you
only plan on performing `twitter search` calls. See [Twitter
OAuth](https://dev.twitter.com/oauth) for more details on how to acquire these
credentials. The service takes these details in from a file, the
following is the format of the file along with required fields:
```
{ 
"Config": {
  "ConsumerKey": "",
  "ConsumerSecret": "",
}
"Token":{
  "Token":"",
  "TokenSecret":"",
},
}
```

## Install

go get -v bitbucket.org/jvulic/twitter

## Usage

Starting the service.

```
twitter start // Start the default Twitter service
twitter start --certd="cert" // Start the Twitter service sourcing service certificates from cert/
twitter start --twcred=creds.json // Start the Twitter service sourcing 'creds.json' for Twitter API credentials
twitter start --loglevel=0 // Start the Twitter service at DEBUG logging level
twitter start --port=1337 // Start the Twitter service listening on port 1337
```

Performing operations on a running service.

```
// Search for tweets containing 'shrek' using the Twitter service located at
// address :1337 with logging level DEBUG.
twitter search "shrek" --target=":1337" --loglevel=0

// Access the Twitter's Streaming API searching for tweets containing 'shrek'
// 'ogre' using the service located at address :1337 with logging level DEBUG.
twitter stream "shrek,ogre" --target=":1337" --loglevel=0
```

Details about each operation, there supported flags, and anything else just add
the `-h` flag.

## Tests

Tests can be run by executing `make test`.

## License
[MIT License](LICENSE)