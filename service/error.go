package service

import (
	"fmt"
)

// TwitterError represents a Twitter API Error response.
// https://dev.twitter.com/overview/api/response-codes
type TwitterError struct {
	Errors []apierror `json:"errors"`
}

type apierror struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
}

func (e TwitterError) Error() string {
	if len(e.Errors) > 0 {
		err := e.Errors[0]
		return fmt.Sprintf("Twitter API error: %d %v", err.Code, err.Message)
	}
	return ""
}

func activeError(errs ...error) error {
	for _, err := range errs {
		switch e := err.(type) {
		case TwitterError:
			if len(e.Errors) != 0 {
				return e
			}
		default:
			if e != nil {
				return e
			}
		}
	}
	return nil
}
