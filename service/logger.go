package service

import (
	"bitbucket.org/jvulic/twitter/service/context/request"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

func ctxLogger(ctx context.Context) *log.Entry {
	if r, ok := request.FromContext(ctx); ok {
		return log.WithFields(log.Fields{
			"request_id":  r.ID,
			"method_name": r.MethodName,
		})
	}
	return log.NewEntry(log.StandardLogger())
}
