/*
Package stream wraps connections to the Twitter STREAM API to simplify access.
*/
package stream

import (
	"bufio"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"github.com/jpillora/backoff"

	"bitbucket.org/jvulic/channel"
)

// Stream is a wrapper the simplifies access to the underlying HTTP client
// connected to the Twitter STREAM API.
type Stream struct {
	opts options

	pipe   *channel.Channel
	err    error
	stop   chan struct{}
	client *http.Client
}

func newExponentialBackoff() *backoff.Backoff {
	return &backoff.Backoff{
		Min:    10 * time.Second,
		Max:    320 * time.Second,
		Factor: 2,
		Jitter: true,
	}
}

func newAggressiveBackoff() *backoff.Backoff {
	return &backoff.Backoff{
		Min:    1 * time.Minute,
		Max:    5 * time.Minute,
		Factor: 2,
		Jitter: true,
	}
}

// scanLines is a split function for a Scanner that returns each line of text
// stripped of the end-of-line marker '\r\n' used by Twitter Streaming APIs.
// This differs from the bufio.ScanLines split function which considers the
// '\r' optional.
// https://dev.twitter.com/streaming/overview/processing
func scanLines(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}

	if i := strings.Index(string(data), "\r\n"); i >= 0 {
		// We have a full '\r\n' terminated line.
		return i + 2, data[0:i], nil
	}

	// If we're at EOF, we have a final, non-terminated line. Return it.
	if atEOF {
		// Remove '\n' if it exists.
		if len(data) > 0 && data[len(data)-1] == '\n' {
			data = data[0 : len(data)-1]
		}
		return len(data), data, nil
	}
	// Request more data.
	return 0, nil, nil
}

type options struct {
	expBackoff     *backoff.Backoff
	aggBackoff     *backoff.Backoff
	channelOptions []channel.Option
}

// Option configures aspects of the stream.
type Option func(*options)

// ExponentialBackoff returns an Option that sets the exponential backoff.
func ExponentialBackoff(b *backoff.Backoff) Option {
	return func(o *options) {
		o.expBackoff = b
	}
}

// AggressiveBackoff returns an Option that sets the aggressive exponential
// backoff.
func AggressiveBackoff(b *backoff.Backoff) Option {
	return func(o *options) {
		o.aggBackoff = b
	}
}

// ChannelOptions returns an Option that configures the internal channel with
// the given []channel.Option.
func ChannelOptions(opts ...channel.Option) Option {
	return func(o *options) {
		o.channelOptions = opts
	}
}

// New creates a new Stream that performs the given request on the given
// authenticated client.
func New(client *http.Client, req *http.Request, opt ...Option) *Stream {
	var opts options
	for _, o := range opt {
		o(&opts)
	}
	if opts.expBackoff == nil {
		// Set the default exponential backoff.
		opts.expBackoff = newExponentialBackoff()
	}
	if opts.aggBackoff == nil {
		// Set the default aggressive exponential backoff.
		opts.aggBackoff = newAggressiveBackoff()
	}

	stream := &Stream{
		opts:   opts,
		pipe:   channel.New(opts.channelOptions...),
		stop:   make(chan struct{}),
		client: client,
	}

	go stream.do(req)
	return stream
}

func (s *Stream) read(body io.ReadCloser) {
	defer body.Close()

	// A bufio.Scanner steps through 'tokens' of data on each Scan(). It returns
	// the number of bytes to advance, slice of token bytes, and any errors.
	scanner := bufio.NewScanner(body)

	// The default ScanLines SplitFunc within the bufio.Scanner does work with
	// Twitter streams.
	scanner.Split(scanLines)
	for scanner.Scan() {
		token := scanner.Bytes()
		if len(token) == 0 {
			// Empty keep-alive
			continue
		}

		// Create a clone of the bytes array before sending it down the channel to
		// let it continue useful work without side-effects.
		clone := make([]byte, len(token))
		copy(clone, token)
		select {
		case <-s.stop:
			return
		case s.pipe.In() <- clone:
		}
	}
}

func (s *Stream) do(req *http.Request) {
	defer s.pipe.Close()

	var wait time.Duration
	for {
		resp, err := s.client.Do(req)
		if err != nil {
			// We stop for HTTP protocol errors.
			s.err = fmt.Errorf("HTTP protocol error: %v", err)
			return
		}
		defer resp.Body.Close()

		switch resp.StatusCode {
		case 200:
			// Received stream response body.
			s.read(resp.Body)
			s.opts.expBackoff.Reset()
			s.opts.aggBackoff.Reset()
		case 503:
			// Exponential backoff.
			wait = s.opts.expBackoff.Duration()
		case 420, 429:
			// Aggressive exponential backoff.
			wait = s.opts.aggBackoff.Duration()
		default:
			// For any other response status code we stop trying.
			s.err = fmt.Errorf("Bad response code: %v", resp.StatusCode)
			return
		}

		// TODO(jv): Consider including a way to specify a backoff threshold or
		// perhaps some meaningful way to get an impression of how long it has been
		// trying to perform a request.

		select {
		case <-s.stop:
			return
		case <-time.After(wait):
			resp.Body.Close() // Close the response body before each retry
		}
	}
}

// Recv provides raw access to the output of the stream. Useful when wanting to
// range over the output channel, or perform selection over many channels
// simultaneously.
func (s *Stream) Recv() <-chan interface{} {
	return s.pipe.Out()
}

// Err returns the stream error if any. Subsequent calls return the same error.
func (s *Stream) Err() error {
	return s.err
}

// Stop stops the Stream. Must be called if volentarily stopping the stream to
// allows the internal goroutine to exit.
func (s *Stream) Stop() {
	close(s.stop)
}
