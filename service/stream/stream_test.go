package stream

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"reflect"
	"strings"
	"testing"
	"time"

	"github.com/dghubble/sling"
	"github.com/jpillora/backoff"

	pb "bitbucket.org/jvulic/twitter/proto"
)

// Returns an http Client, ServeMux, and Server. The client proxies requests to
// the server where handlers can be registered (using the mux) to handle the
// requests.  The caller must close the test server.
func testServer() (*http.Client, *http.ServeMux, *httptest.Server) {
	mux := http.NewServeMux()
	server := httptest.NewServer(mux)
	transport := &RewriteTransport{&http.Transport{
		Proxy: func(req *http.Request) (*url.URL, error) {
			return url.Parse(server.URL)
		},
	}}
	client := &http.Client{Transport: transport}
	return client, mux, server
}

// RewriteTransport rewrites the https requests to http to avoid TLS cert
// issues.
type RewriteTransport struct {
	Transport http.RoundTripper
}

func (t *RewriteTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	req.URL.Scheme = "http"
	if t.Transport == nil {
		return http.DefaultTransport.RoundTrip(req)
	}
	return t.Transport.RoundTrip(req)
}

func assertMethod(expectedMethod string, req *http.Request) error {
	if req.Method != expectedMethod {
		return fmt.Errorf("Incorrect http method: %v", req.Method)
	}
	return nil
}

func assertQuery(expected map[string]string, req *http.Request) error {
	queryValues := req.URL.Query()
	expectedValues := url.Values{}
	for key, value := range expected {
		expectedValues.Add(key, value)
	}

	if !reflect.DeepEqual(queryValues, expectedValues) {
		return fmt.Errorf("Unexpected query: %v != %v", queryValues, expectedValues)
	}
	return nil
}

type token struct {
	Text string
}

func toToken(data interface{}) (*token, error) {
	switch x := data.(type) {
	case []byte:
		tok := new(token)
		if err := json.Unmarshal(x, tok); err != nil {
			return nil, fmt.Errorf("Failed to unmarshal JSON token: %s", err)
		}
		return tok, nil
	default:
		return nil, fmt.Errorf("Unexpected type: %T", x)
	}
}

func TestBasic(t *testing.T) {
	httpClient, mux, server := testServer()
	defer server.Close()

	requestCount := 0
	mux.HandleFunc("/1.1/statuses/filter.json", func(w http.ResponseWriter, r *http.Request) {
		if err := assertMethod("POST", r); err != nil {
			t.Fatal(err)
		}
		if err := assertQuery(map[string]string{"track": "shrek,shrek is love"}, r); err != nil {
			t.Fatal(err)
		}

		switch requestCount {
		case 0:
			w.Header().Set("Content-Type", "application/json")
			w.Header().Set("Transfer-Encoding", "chunked")
			fmt.Fprintf(w,
				`{"text": "shrek is love!"}`+"\r\n"+
					`{"text": "shrek is life!"}`+"\r\n",
			)
		default:
			// Only do one round.
			http.Error(w, "Stream API not available!", 130)
		}
		requestCount++
	})

	req, err := sling.New().
		Client(httpClient).
		Base("https://stream.twitter.com/1.1/").Path("statuses/").Post("filter.json").
		QueryStruct(&pb.StreamTweetsArgs{
			Track: []string{"shrek", "shrek is love"},
		}).Request()
	if err != nil {
		t.Fatalf("Failed to create request object: %v", err)
	}

	stream := New(httpClient, req)
	defer stream.Stop()
	for data := range stream.Recv() {
		tok, err := toToken(data)
		if err != nil {
			t.Fatalf("Failed to convert data to token: %v", tok)
		}

		if !strings.Contains(tok.Text, "shrek") {
			t.Fatalf("Unexpected response token: %v", tok)
		}
	}
}

func TestBackoff(t *testing.T) {
	httpClient, mux, server := testServer()
	defer server.Close()

	requestCount := 0
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		switch requestCount {
		case 0:
			http.Error(w, "Service Unavailable", 503)
		case 1:
			http.Error(w, "Too Many Requests", 429)
		default:
			http.Error(w, "Stream API not available!", 130)
		}
		requestCount++
	})

	expboff := &backoff.Backoff{
		Min:    1 * time.Nanosecond,
		Max:    1 * time.Second,
		Factor: 2,
	}
	aggboff := &backoff.Backoff{
		Min:    1 * time.Nanosecond,
		Max:    1 * time.Second,
		Factor: 2,
	}
	req, _ := http.NewRequest("GET", "http://example.com/", nil)
	stream := New(httpClient, req, ExponentialBackoff(expboff), AggressiveBackoff(aggboff))
	defer stream.Stop()
	for _ = range stream.Recv() {
		// Loop until the stream exits.
	}

	if expboff.Attempt() != 1 && aggboff.Attempt() != 1 {
		t.Fatalf("Unexpected backoff attempts: expboff=%v aggboff=%v", expboff.Attempt(), aggboff.Attempt())
	}
}
