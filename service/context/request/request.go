package request

import (
	"github.com/pborman/uuid"
	"golang.org/x/net/context"
)

// Request defines common details of a rpc request.
type Request struct {
	ID         uuid.UUID
	MethodName string
}

type requestKey struct{}

// NewContext returns a new context containing the given Request object.
func NewContext(ctx context.Context, r *Request) context.Context {
	return context.WithValue(ctx, requestKey{}, r)
}

// FromContext retrieves the Request object from the given context, if any.
func FromContext(ctx context.Context) (r *Request, ok bool) {
	r, ok = ctx.Value(requestKey{}).(*Request)
	return
}
