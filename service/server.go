/*
Package service provides an implemenation of the twitter service defined under
package proto.
*/
package service

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"

	"github.com/dghubble/sling"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"

	"bitbucket.org/jvulic/channel"
	pb "bitbucket.org/jvulic/twitter/proto"
	twstream "bitbucket.org/jvulic/twitter/service/stream"
)

const gBaseTwitterAPI = "https://api.twitter.com/1.1/"
const gBasePublicStream = "https://stream.twitter.com/1.1/"

// Server is a grpc complient server as defined in /proto.
type Server struct {
	client *http.Client
	api    *sling.Sling
	public *sling.Sling
}

// NewTwitterServer creates a new grpc complient server as defined in /proto.
func NewTwitterServer(httpClient *http.Client) *Server {
	return &Server{
		client: httpClient,
		api:    sling.New().Client(httpClient).Base(gBaseTwitterAPI),
		public: sling.New().Client(httpClient).Base(gBasePublicStream).Path("statuses/"),
	}
}

// SearchTweets queries the Twitter REST API.
func (s *Server) SearchTweets(ctx context.Context, args *pb.SearchTweetsArgs) (*pb.SearchTweetsRet, error) {
	clog := ctxLogger(ctx)

	// Query must be escaped before use.
	args.Query = url.QueryEscape(args.Query)
	req, err := s.api.New().Path("search/").Get("tweets.json").QueryStruct(args).Request()
	if err != nil {
		clog.WithError(err).Error("Failed to create request object")
		return nil, errors.New("Failed to create request object")
	}

	clog.WithField("request", req).Info("Querying Twitter Tweet Search API")

	ret := new(pb.SearchTweetsRet)
	apiErr := TwitterError{}
	resp, err := s.api.Do(req, ret, apiErr)
	if active := activeError(err, apiErr); active != nil {
		clog.WithFields(log.Fields{
			"api_error": apiErr,
			"error":     err,
			"active":    active,
		}).Error("Failed to query Twitter Tweet search API")
		return nil, errors.New("Failed to query Twitter Tweet search API")
	}

	// Check response code for completeness (success=2XX).
	if code := resp.StatusCode; code < 200 || code > 299 {
		clog.WithField("status_code", code).Error("Received non-success response code")
		return nil, errors.New("Received non-success response code")
	}
	return ret, nil
}

func toStreamTweetsRet(data interface{}) (*pb.StreamTweetsRet, error) {
	switch x := data.(type) {
	case []byte:
		tweet := new(pb.Tweet)
		if err := json.Unmarshal(x, tweet); err != nil {
			return nil, fmt.Errorf("Failed to unmarshal JSON encoded tweet: %s", err)
		}
		return &pb.StreamTweetsRet{Status: tweet}, nil
	default:
		return nil, fmt.Errorf("Unexpected type: %T", x)
	}
}

// StreamTweets queries the Twitter STREAM API.
func (s *Server) StreamTweets(args *pb.StreamTweetsArgs, stream pb.Twitter_StreamTweetsServer) error {
	ctx := stream.Context()
	clog := ctxLogger(ctx)

	req, err := s.public.New().Post("filter.json").QueryStruct(args).Request()
	if err != nil {
		clog.WithError(err).Error("Failed to create request object")
		return errors.New("Failed to create request object")
	}

	clog.WithField("request", req).Info("Querying Twitter Public Stream API")

	chOpt := twstream.ChannelOptions(
		channel.SizeAlert(50, func(size int) {
			clog.WithField("queue_size", size).Warning("Twitter channel queue threshold exceeded")
		}))
	twitterStream := twstream.New(s.client, req, chOpt)
	defer twitterStream.Stop()
	for {
		select {
		case data, ok := <-twitterStream.Recv():
			if !ok {
				// Stream has been closed.
				clog.WithError(twitterStream.Err()).Error("Failed to receive data from stream")
				return errors.New("Failed to receive data from stream")
			}
			ret, err := toStreamTweetsRet(data)
			if err != nil {
				clog.WithError(err).Error("Failed to prase stream token")
				return errors.New("Failed to prase stream token")
			}
			clog.WithField("ret", ret).Debug("Received data from Streaming API")

			err = stream.Send(ret)
			if err != nil {
				clog.WithError(err).Error("Failed to send data")
				return errors.New("Failed to send data")
			}
		case <-ctx.Done():
			err := ctx.Err()
			clog.WithError(err).Info("Call context was canceled")
			return errors.New("Context cancel signal received")
		}

	}
}
