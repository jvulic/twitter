package cmd

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"path"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	pb "bitbucket.org/jvulic/twitter/proto"
)

type clientFlags struct {
	insecure      bool
	targetAddress string
	certDir       string
}

var gClientFlags clientFlags

// searchCmd represents the search command
var searchCmd = &cobra.Command{
	Use:   "search",
	Short: "Returns tweets satisfying the query filter predicates",
	Long: `
	twitter search "shrek is an ogre" --target=":50001"
	`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 1 {
			log.Fatal("You must provide a search query: twitter search \"example\"")
		}
		query := args[0]

		var conn *grpc.ClientConn
		var err error
		if gClientFlags.insecure {
			conn, err = grpc.Dial(gClientFlags.targetAddress, grpc.WithInsecure())
		} else {
			tlsConf, err := createClientTLSConfig(gClientFlags.certDir)
			if err != nil {
				log.WithError(err).Fatal("Failed to create TLS config")
			}
			conn, err = grpc.Dial(gClientFlags.targetAddress, grpc.WithTransportCredentials(credentials.NewTLS(tlsConf)))
		}
		if err != nil {
			log.WithFields(log.Fields{
				"error":   err,
				"address": gClientFlags.targetAddress,
			}).Fatal("Failed to dial remote")
		}
		defer conn.Close()

		client := pb.NewTwitterClient(conn)
		// TODO(jv): Add support for the other search params (result_type etc.)
		data, err := client.SearchTweets(context.Background(), &pb.SearchTweetsArgs{Query: query})
		if err != nil {
			log.WithFields(log.Fields{
				"error": err,
			}).Fatal("Failed rpc call")
		}

		log.WithFields(log.Fields{
			"result": data,
		}).Info("Successfully queried the twitter search endpoint")
	},
}

// streamCmd represents the search command
var streamCmd = &cobra.Command{
	Use:   "stream",
	Short: "Streams tweets satisfiying the track filter predicates",
	Long: `
	twitter stream "shrek,ogre" --target=":50001"
	`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 1 {
			log.Fatal("You must provide track parameters: twitter stream \"example\"")
		}
		track := args[0]

		var conn *grpc.ClientConn
		var err error
		if gClientFlags.insecure {
			conn, err = grpc.Dial(gClientFlags.targetAddress, grpc.WithInsecure())
		} else {
			tlsConf, err := createClientTLSConfig(gClientFlags.certDir)
			if err != nil {
				log.WithError(err).Fatal("Failed to create TLS config")
			}
			conn, err = grpc.Dial(gClientFlags.targetAddress, grpc.WithTransportCredentials(credentials.NewTLS(tlsConf)))
		}
		if err != nil {
			log.WithFields(log.Fields{
				"error":   err,
				"address": gClientFlags.targetAddress,
			}).Fatal("Failed to dial remote")
		}
		defer conn.Close()

		client := pb.NewTwitterClient(conn)
		ctx, cancel := context.WithCancel(context.Background())
		stream, err := client.StreamTweets(ctx, &pb.StreamTweetsArgs{Track: strings.Split(track, ",")})
		if err != nil {
			log.WithFields(log.Fields{
				"error": err,
			}).Fatal("Failed initial rpc call")
		}

		for i := 0; i < 3; i++ {
			data, err := stream.Recv()
			if err != nil {
				log.WithFields(log.Fields{
					"error": err,
				}).Fatal("Error receiving from stream")
			}

			log.WithFields(log.Fields{
				"result": data,
			}).Info("Received twitter data")
		}

		// Cancel's the rpc.
		cancel()
	},
}

func init() {
	gClientFlags = clientFlags{}

	rootCmd.AddCommand(searchCmd)
	searchCmd.Flags().BoolVar(&gClientFlags.insecure, "insecure", false, "Toggle insecure communication")
	searchCmd.Flags().StringVar(&gClientFlags.targetAddress, "target", ":50001", "Address of target twitter server")
	searchCmd.Flags().StringVar(&gClientFlags.certDir, "certdir", "/twitter/cert/", "Path to certificate directory; Matches ca.pem, twitter.pem, and twitter.key within the directory")

	rootCmd.AddCommand(streamCmd)
	streamCmd.Flags().BoolVar(&gClientFlags.insecure, "insecure", false, "Toggle insecure communication")
	streamCmd.Flags().StringVar(&gClientFlags.targetAddress, "target", ":50001", "Address of target twitter server")
	streamCmd.Flags().StringVar(&gClientFlags.certDir, "certdir", "/twitter/cert/", "Path to certificate directory; Matches ca.pem, twitter.pem, and twitter.key within the directory")
}

func createClientTLSConfig(dir string) (*tls.Config, error) {
	cert, err := tls.LoadX509KeyPair(
		path.Join(dir, "twitter.pem"),
		path.Join(dir, "twitter.key"))
	if err != nil {
		return nil, fmt.Errorf("Failed to load cert/key: %v", err)
	}

	caCert, err := ioutil.ReadFile(path.Join(dir, "ca.pem"))
	if err != nil {
		return nil, fmt.Errorf("Failed to read ca certificate: %v", err)
	}

	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)
	return &tls.Config{
		Certificates: []tls.Certificate{cert},
		RootCAs:      caCertPool,
		ServerName:   "twitter",
	}, nil
}
