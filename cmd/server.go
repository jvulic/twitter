package cmd

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"path"

	"github.com/dghubble/oauth1"
	"github.com/pborman/uuid"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"golang.org/x/net/context"
	"golang.org/x/net/trace"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/tap"

	pb "bitbucket.org/jvulic/twitter/proto"
	"bitbucket.org/jvulic/twitter/service"
	"bitbucket.org/jvulic/twitter/service/context/request"
)

func inHandle(ctx context.Context, info *tap.Info) (context.Context, error) {
	// Generate a request id for use within the service scope.
	rid := uuid.NewRandom()

	// Store request within the context.
	r := &request.Request{
		ID:         rid,
		MethodName: info.FullMethodName,
	}
	ctx = request.NewContext(ctx, r)

	// Update trace.
	if tr, ok := trace.FromContext(ctx); ok {
		tr.LazyPrintf("assigned request id: %v", rid)
	}
	return ctx, nil
}

type oauthCredentials struct {
	Config *oauth1.Config
	Token  *oauth1.Token
}

type serverFlags struct {
	insecure        bool
	address         string
	twitterCredPath string
	certDir         string
}

var gServerFlags serverFlags

// startCmd represents the server command
var startCmd = &cobra.Command{
	Use:   "start",
	Short: "Starts an instance of the Twitter service",
	Long: `
		twitter start --addr=":50001" --twcred="twitter-cred.json"
	`,
	Run: func(cmd *cobra.Command, args []string) {
		lis, err := net.Listen("tcp", gServerFlags.address)
		if err != nil {
			log.WithFields(log.Fields{
				"error":   err,
				"address": gServerFlags.address,
			}).Fatal("Failed to listen")
		}

		var server *grpc.Server
		if gServerFlags.insecure {
			server = grpc.NewServer(grpc.InTapHandle(inHandle))
		} else {
			tlsConf, err := createServerTLSConfig(gServerFlags.certDir)
			if err != nil {
				log.WithError(err).Fatal("Failed to create TLS config")
			}
			server = grpc.NewServer(grpc.InTapHandle(inHandle), grpc.Creds(credentials.NewTLS(tlsConf)))
		}

		// Create and configure Twitter HTTP client.
		httpClient, err := createTwitterHTTPClientFromPath(gServerFlags.twitterCredPath)
		if err != nil {
			log.WithError(err).Fatal("Failed to create http client")
		}

		pb.RegisterTwitterServer(server, service.NewTwitterServer(httpClient))
		log.WithField("address", lis.Addr().String()).Info("Starting the twitter server")
		server.Serve(lis)
	},
}

func init() {
	gServerFlags = serverFlags{}

	rootCmd.AddCommand(startCmd)
	startCmd.Flags().BoolVar(&gServerFlags.insecure, "insecure", false, "Toggle insecure communication")
	startCmd.Flags().StringVar(&gServerFlags.address, "addr", ":50001", "address to listen on")
	startCmd.Flags().StringVar(&gServerFlags.twitterCredPath, "twcred", "/twitter/twitter-cred.json", "Path to config file containing Twitter API credentials")
	startCmd.Flags().StringVar(&gServerFlags.certDir, "certdir", "/twitter/cert/", "Path to certificate directory; Matches ca.pem, twitter.pem, and twitter.key within the directory")
}

func createTwitterHTTPClientFromPath(dir string) (*http.Client, error) {
	b, err := ioutil.ReadFile(dir)
	if err != nil {
		return nil, err
	}

	var creds oauthCredentials
	err = json.Unmarshal(b, &creds)
	if err != nil {
		return nil, err
	}

	config := creds.Config
	token := creds.Token
	return config.Client(oauth1.NoContext, token), nil
}

func createServerTLSConfig(dir string) (*tls.Config, error) {
	cert, err := tls.LoadX509KeyPair(
		path.Join(dir, "twitter.pem"),
		path.Join(dir, "twitter.key"))
	if err != nil {
		return nil, fmt.Errorf("Failed to load cert/key: %v", err)
	}

	caCert, err := ioutil.ReadFile(path.Join(dir, "ca.pem"))
	if err != nil {
		return nil, fmt.Errorf("Failed to read ca certificate: %v", err)
	}

	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)
	return &tls.Config{
		Certificates: []tls.Certificate{cert},
		ClientCAs:    caCertPool,
		ClientAuth:   tls.RequireAndVerifyClientCert,
	}, nil
}
