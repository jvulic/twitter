package cmd

import (
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

type rootFlags struct {
	logLevel int
}

var gRootFlags rootFlags

var rootCmd = &cobra.Command{
	Use: "twitter",
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func init() {
	cobra.OnInitialize(initRoot)

	gRootFlags = rootFlags{}
	rootCmd.PersistentFlags().IntVar(&gRootFlags.logLevel, "loglevel", 1, "Desired logging level")
}

// Applies any root-level flags and actions.
func initRoot() {
	// Apply loglevel to Logrus.
	var level log.Level
	switch gRootFlags.logLevel {
	case 0:
		level = log.DebugLevel
	case 1:
		level = log.InfoLevel
	case 2:
		level = log.WarnLevel
	case 3:
		level = log.ErrorLevel
	case 4:
		level = log.FatalLevel
	case 5:
		level = log.PanicLevel
	default:
		level = log.DebugLevel
	}
	log.SetLevel(level)
}
