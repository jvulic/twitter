spell:
	@ if ! which misspell > /dev/null; then \
			echo "error: misspell not installed" >&2; \
			exit 1; \
		fi
	@ misspell -w .

lint:
	@ if ! which gometalinter > /dev/null; then \
			echo "error: gometalinter not installed" >&2; \
			exit 1; \
		fi
	@ gometalinter --exclude=proto --disable-all --enable=golint --enable=vet --enable=gofmt ./... 

proto:
	@ if ! which protoc > /dev/null; then \
			echo "error: protoc not installed" >&2; \
			exit 1; \
		fi
	@ # Use $$dir as the root for all proto files in the same directory.
	@ for dir in $$(find . -not -path "./protobuf/*" -name '*.proto' | xargs -n1 dirname | uniq); do \
			protoc -I $$dir --go_out=plugins=grpc:$$dir $$dir/*.proto; \
		done
	@ # Apply proto-go-inject-tag
	@ if ! which protoc-go-inject-tag > /dev/null; then \
			echo "error: protoc-go-inject-tag not installed" >&2; \
			exit 1; \
		fi
	@ # '> /dev/null' maps stdcout to /dev/null and '2>&1' maps stderr to stdout
	@ for file in $$(find . -name '*.pb.go' | uniq); do \
			protoc-go-inject-tag -input=$$file > /dev/null 2>&1; \
		done

test:
	go test ./service/stream

.PHONY: \
	spell \
	lint \
	proto \
	test \
